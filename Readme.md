# OpenWebUI & Ollama

Bienvenue dans le projet OpenWebUI & Ollama, une interface web avancée conçue pour interagir avec des chatbots IA. Ce projet utilise "Open WebUI" (anciennement Ollama WebUI) et étend ses capacités pour intégrer des fonctionnalités comme la gestion des Large Language Models (LLM) et la compatibilité avec les API Ollama et OpenAI.

Vous pouvez retrouvé le Readme.md de "Open WebUI (Formerly Ollama WebUI)" dans le dossier du code source à la racine du repository "./OpenWebUI_ollama/readme.md".

Vous retrouverez également, à la racine du projet, la documentation technique et explicative de notre projet (rendu cours DevOps Groupe 2).

## Démarrage rapide

Le pipeline CI/CD se charge de déployer automatiquement l'infrastructure nécessaire sur Scaleway ainsi que l'application ChatBot Open Web UI. Retrouvez plus de détails dans le fichier Readme.md du dossier ./OpenWebUI_ollama à la racine du répertoire.

Pour accéder à l'application, utilisez l'URL fournie par l'adresse IP externe du service de Load Balancing du pod OpenWebUI :
```bash
<IP:8080>
```

Une fois sur la page d'accueil d'Open Web UI, créez un compte pour débuter vos interactions avec le chatbot. L'utilisation d'une adresse email fictive est fonctionnelle pour cette étape.

## Utilisation du ChatBot avec des modèles !

Nous recommandons d'essayer "Tinyllama", un modèle compact avec 1,1 milliard de paramètres, idéal pour les applications nécessitant une utilisation minimale des ressources.

Pour ajouter ce modèle :

Rendez-vous dans les Paramètres, accessible en bas à gauche.
Sélectionnez l'onglet Modèles.
Dans "Ajouter un modèle", saisissez "tinyllama".
Profitez de votre chatbot avec le nouveau modèle !

![Open WebUI Demo](./demo.gif)

## Installation

Le fichier gitlab-ci.yaml orchestre les tests, le build et le déploiement de l'application ainsi que l'infrastructure sous-jacente sur Scaleway avec Kubernetes. Ce processus inclut la mise en place de l'application via Helm, le Load Balancing et le monitoring. 

Environnements
Pré-production : Instructions pour déployer dans un environnement de pré-production.

```bash
pip install foobar
```

Production : Instructions pour le déploiement en production.
```bash
pip install foobar
```

## README a terminé ! 

## Usage (Fichiers de configuration etc...)

```python
import foobar

# returns 'words'
foobar.pluralize('word')

# returns 'geese'
foobar.pluralize('goose')

# returns 'phenomenon'
foobar.singularize('phenomena')
```

## Packaging Variables 

Voici comment nous avons éxploité nos variables d'environnement dans Gitlab.


## License

[MIT](https://choosealicense.com/licenses/mit/)