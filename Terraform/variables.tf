variable "SCALEWAY_ACCESS_KEY" {
  description = "Access Key for Scaleway API"
  type        = string
}

variable "SCALEWAY_SECRET_KEY" {
  description = "Secret Key for Scaleway API"
  type        = string
}

variable "SCALEWAY_PROJECT_ID" {
  description = "Project ID for Scaleway API"
  type        = string
}
