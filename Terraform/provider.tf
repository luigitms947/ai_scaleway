terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
    }
  }
  required_version = ">= 0.13"
}

provider "scaleway" {
  access_key = var.SCALEWAY_ACCESS_KEY
  secret_key = var.SCALEWAY_SECRET_KEY
  project_id = var.SCALEWAY_PROJECT_ID
  zone       = "fr-par-1"
  region     = "fr-par"
}
